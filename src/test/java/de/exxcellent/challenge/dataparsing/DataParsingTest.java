package de.exxcellent.challenge.dataparsing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.exxcellent.challenge.data.parsing.DataParser;

public class DataParsingTest {

	DataParser weatherDataParser;

	@Before
	public void setUp() throws Exception {
		weatherDataParser = new DataParser();
	}

	@Test
	public void testFileNotFound() {
		weatherDataParser = new DataParser();
		assertNull(weatherDataParser.parsData("fileThatNotExists.csv", "", "", ""));
	}

	@Test
	public void testEmptyFile() {

		weatherDataParser = new DataParser();
		assertTrue(weatherDataParser.parsData("src/test/java/de/exxcellent/challenge/dataparsing/empty.csv", "", "", "").isEmpty());
	}

	@Test
	public void testFileWith4Lines() {
		weatherDataParser = new DataParser();
		assertTrue(weatherDataParser.parsData("src/test/java/de/exxcellent/challenge/dataparsing/testWithFourLines.csv", "Day", "MxT", "MnT").size() == 4);
	}

	@Test
	public void testFileWithRedundantLines() {
		weatherDataParser = new DataParser();
		assertTrue(weatherDataParser.parsData("src/test/java/de/exxcellent/challenge/dataparsing/testWithRedundantValues.csv", "Day", "MxT", "MnT").size() == 4);
	}


}
