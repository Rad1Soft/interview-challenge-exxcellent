package de.exxcellent.challenge.dataprocessing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import de.exxcellent.challenge.data.parsing.IMinMaxData;
import de.exxcellent.challenge.data.parsing.model.MinMaxData;
import de.exxcellent.challenge.data.processing.AbsoluteMinimalDifferenceCalculator;


class dataProcessingTest {
	
	HashSet<IMinMaxData> setDataTest;
	AbsoluteMinimalDifferenceCalculator weatherTempuratureSpread;
	
	@Before
	public void setUp() throws Exception {
		setDataTest = new HashSet<IMinMaxData>();
		weatherTempuratureSpread = new AbsoluteMinimalDifferenceCalculator();
	}

	@Test
	void testSetIsNull() {
		setDataTest = new HashSet<IMinMaxData>();
		weatherTempuratureSpread = new AbsoluteMinimalDifferenceCalculator();
		assertTrue(weatherTempuratureSpread.findTheMinimumAbsoluteDifference(setDataTest).isEmpty());
	}
	
	@Test
	void testWith3Days() {
		setDataTest = new HashSet<IMinMaxData>();
		weatherTempuratureSpread = new AbsoluteMinimalDifferenceCalculator();
		setDataTest.add(new MinMaxData("25",30,15));
		setDataTest.add(new MinMaxData("20",25,20));
		setDataTest.add(new MinMaxData("5",13,12));
		
		assertEquals("5", weatherTempuratureSpread.findTheMinimumAbsoluteDifference(setDataTest));

	}
	
	@Test
	void testWithOneDay() {
		setDataTest = new HashSet<IMinMaxData>();
		weatherTempuratureSpread = new AbsoluteMinimalDifferenceCalculator();
		setDataTest.add(new MinMaxData("10",30,15));
		assertEquals("10", weatherTempuratureSpread.findTheMinimumAbsoluteDifference(setDataTest));
	}
	
	@Test
	void testSetWith4Days() {
		setDataTest = new HashSet<IMinMaxData>();
		weatherTempuratureSpread = new AbsoluteMinimalDifferenceCalculator();
		setDataTest.add(new MinMaxData("25",30,15));
		setDataTest.add(new MinMaxData("20",25,20));
		setDataTest.add(new MinMaxData("11",15,12));
		setDataTest.add(new MinMaxData("5",19,12));
		
		assertEquals("11", weatherTempuratureSpread.findTheMinimumAbsoluteDifference(setDataTest));
	}
	

}
