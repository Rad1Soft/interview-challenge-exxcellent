package de.exxcellent.challenge.data.parsing;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.exxcellent.challenge.data.parsing.model.MinMaxData;

/**
 * The class for parsing a CSV File
 *
 * @author Radhouan Ben Bahri
 */

public class DataParser implements IDataFetching {

	@Override
	public HashSet<IMinMaxData> parsData(String fileName, String columnName, String maxValue, String minValue) {

		CSVParser parser = null;
		HashSet<IMinMaxData> setOfDataValues = new HashSet<IMinMaxData>();


		try {
			parser = new CSVParser(new FileReader(fileName), CSVFormat.DEFAULT.withHeader());
		} catch (FileNotFoundException e) {
			System.out.println("The given file with name " + fileName +" was not found");
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		for (CSVRecord record : parser) {
			String  objectName = record.get(columnName);
			Integer minTemp = 0;
			Integer maxTemp = 0;
			try {
				maxTemp = Integer.parseInt(record.get(maxValue));
				minTemp = Integer.parseInt(record.get(minValue));
				setOfDataValues.add(new MinMaxData(objectName, maxTemp, minTemp));
				
			} catch (Exception e) {
				System.out.println("The minimum value or the Maximum value of " + record.get(columnName) + " has a wrong format ");
			}
		}
	
		try {
			parser.close();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return setOfDataValues;
	}

}
