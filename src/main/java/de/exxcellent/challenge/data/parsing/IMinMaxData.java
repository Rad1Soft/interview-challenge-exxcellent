package de.exxcellent.challenge.data.parsing;

/**
 * Interface IMinMaxData
 * Defining the Structure of the Data given as input
 * 
 * @author Radhouan Ben Bahri
 */

public interface IMinMaxData {
	
	public String getName();
	
	public Integer getMaxValue();
	
	public Integer getMinValue();

}
