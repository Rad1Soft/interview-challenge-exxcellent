package de.exxcellent.challenge.data.parsing;

import java.util.HashSet;

/**
 * Interface for parsing the Data
 *
 * @author Radhouan Ben Bahri
 */

public interface IDataFetching {

	public HashSet<IMinMaxData> parsData(String fileName, String columnName, String maxValue, String minValue);

}
