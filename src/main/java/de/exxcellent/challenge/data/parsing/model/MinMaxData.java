package de.exxcellent.challenge.data.parsing.model;

import de.exxcellent.challenge.data.parsing.IMinMaxData;

public class MinMaxData implements IMinMaxData{
	
	private String dataName;
	
	private Integer maxValue;
	
	private Integer minValue;
	
	public MinMaxData(String dataName, Integer maxValue, Integer minValue) {
		
		this.dataName = dataName;
		this.maxValue= maxValue;
		this.minValue = minValue;
	}
	
	@Override
	public String getName() {
		
		return dataName;
	}

	@Override
	public Integer getMaxValue() {
		return maxValue;
	}

	@Override
	public Integer getMinValue() {
		return minValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataName == null) ? 0 : dataName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MinMaxData other = (MinMaxData) obj;
		if (dataName == null) {
			if (other.dataName != null)
				return false;
		} else if (!dataName.equals(other.dataName))
			return false;
		return true;
	}
	


}
