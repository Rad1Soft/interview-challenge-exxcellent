package de.exxcellent.challenge.data.processing;

import java.util.HashSet;

import de.exxcellent.challenge.data.parsing.IMinMaxData;


/**
 * The WeatherTempuratureSpread class has the method print() which  gets 
 * a set of objects. These objects will be processed where the minimum of the 
 * absolute difference will be calculated and printed  
 *
 * @author Radhouan Ben Bahri
 */

public class AbsoluteMinimalDifferenceCalculator implements IAbsoluteDifference {

	@Override
	public String findTheMinimumAbsoluteDifference(HashSet<IMinMaxData> dataSet) {

		String absoluteDifference = "";

		if (dataSet == null || dataSet.isEmpty()) {
			System.out.println("There was no Input data");
			return absoluteDifference;
		}
		// initialize the minimum as the difference between  in the first element of the Set
		int minValue = Math.abs(dataSet.stream().findFirst().get().getMaxValue()
				- dataSet.stream().findFirst().get().getMinValue());
		absoluteDifference = dataSet.stream().findFirst().get().getName();
		
		for (IMinMaxData day : dataSet) {
			if (Math.abs(day.getMaxValue() - day.getMinValue()) < minValue) {
				minValue = day.getMaxValue() - day.getMinValue();
				absoluteDifference = day.getName();
			}
		}

		return absoluteDifference;

	}

}
