package de.exxcellent.challenge.data.processing;

import java.util.HashSet;

import de.exxcellent.challenge.data.parsing.IMinMaxData;

/**
 * Interface IAbsoluteDifference
 * Defining the Structure of the Difference Calculator
 * 
 * @author Radhouan Ben Bahri
 */

public interface IAbsoluteDifference {
	
	public String findTheMinimumAbsoluteDifference(HashSet<IMinMaxData> dataSet);

}
