package de.exxcellent.challenge;

import java.util.HashSet;

import de.exxcellent.challenge.data.parsing.IMinMaxData;
import de.exxcellent.challenge.data.parsing.DataParser;
import de.exxcellent.challenge.data.processing.AbsoluteMinimalDifferenceCalculator;

/**
 * The entry class for your solution. This class is only aimed as starting point and not intended as baseline for your software
 * design. Read: create your own classes and packages as appropriate.
 *
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
public final class App {

	public static final String WEATHER_FILE_PATH = "src/main/resources/de/exxcellent/challenge/weather.csv";
	public static final String DAY = "Day";
	public static final String TEMP_MAX_VALUE = "MxT";
	public static final String TEMP_MIN_VALUE = "MnT";
	public static final String FOOTBALL_FILE_PATH = "src/main/resources/de/exxcellent/challenge/football.csv";
	public static final String TEAM = "Team";
	public static final String GOALS = "Goals";
	public static final String ALLOWED_GOALS = "Goals Allowed";
	
    public static void main(String... args) {

    	DataParser weatherDataParser = new DataParser();
    	
    	//Parse the weather File
    	HashSet<IMinMaxData> weatherSet = weatherDataParser.parsData(WEATHER_FILE_PATH, DAY, TEMP_MAX_VALUE, TEMP_MIN_VALUE);
    	
    	//Parse the Football File
    	HashSet<IMinMaxData> footballSet = weatherDataParser.parsData(FOOTBALL_FILE_PATH, TEAM, GOALS, ALLOWED_GOALS);
    	
    	AbsoluteMinimalDifferenceCalculator differenceCalculator = new AbsoluteMinimalDifferenceCalculator();   	

    	// get the Day with the spread temperature
    	 String dayWithSmallestTempSpread =  differenceCalculator.findTheMinimumAbsoluteDifference(weatherSet);
        
    	// get the Team with the smallest goal spread
        String teamWithSmallesGoalSpread = differenceCalculator.findTheMinimumAbsoluteDifference(footballSet);
        
        System.out.printf("Day with smallest temperature spread : %s%n", dayWithSmallestTempSpread);
        System.out.printf("Team with smallest goal spread       : %s%n", teamWithSmallesGoalSpread);
    }
}
